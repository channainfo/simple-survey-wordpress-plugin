<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'L8-l`;lVStu@X~J3xk-g7(@pV~r]+d.+>Wb%JNs9K(;8@.f3t*0,0d_h1@D7.Rf2');
define('SECURE_AUTH_KEY',  ']66y@V@_>%f;H>eF7g.d4W)IzfP}pmWw[!No^kS9ui-]ef#0Vxv62s 3_&3RA.&h');
define('LOGGED_IN_KEY',    'k*p6P-6,>#79<L)la+tM`?sUh~0W-n>;I|M%yB0e_A&5>c)#S<9mwdOy${Oq2ueB');
define('NONCE_KEY',        'JMwDA5/;`&)JORLL4DwXG&t.`8a1vht$2dHOK|Eh+E:J)~K!06sAbuQWSu`G#^>L');
define('AUTH_SALT',        'T<lhH)ep(tDmH{ui~XqAb=is!?)]kzFY8?9uzigV!S4{Y>gJLWxA8(mCsb>qq6+p');
define('SECURE_AUTH_SALT', '_x`oZh>%gcuNpkr>S|XSV(AfYts|78fQ/H8Y7j><k#mQ^Gf?R~}*q/Fp^Kb_+^#2');
define('LOGGED_IN_SALT',   'efq}$r7|8p76H$f[bcLW)mn9[i1q:f7%mzcR%T@J?<KB=GgK.)S6$ZhSfwApK7|W');
define('NONCE_SALT',       '7c;Dv?:X;-AS=gz+2)qz V,vdrFL(`7-s&;ukzlH6L<C#bIgf,M/ysI7q8AY8C:d');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
