// Form input validation
function wpss_checkform(form){

	if (form.name.value == "") {
		alert( "Please enter your name." );
		form.name.focus();
		return false ;
	}
	if (form.lname.value == "") {
		alert( "Please enter your name." );
		form.lname.focus();
		return false ;
	}
	if (form.email.value == "") {
		alert( "Please enter a valid email address." );
		form.email.focus();
		return false ;
	}

	str = form.email.value;
	var at="@";
	var dot=".";
	var lat=str.indexOf(at);
	var lstr=str.length;
	var ldot=str.indexOf(dot);
	if (str.indexOf(at)==-1){
		alert("Please enter a valid email address.");
		return false;
	}

	if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
		alert("Please enter a valid email address.");
		return false;
	}

	if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
		alert("Please enter a valid email address.");
		return false;
	}

	if (str.indexOf(at,(lat+1))!=-1){
		alert("Please enter a valid email address.");
		return false;
	}

	if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
		alert("Please enter a valid email address.");
		return false;
	}

	if (str.indexOf(dot,(lat+2))==-1){
		alert("Please enter a valid email address.");
		return false;
	}

	if (str.indexOf(" ")!=-1){
		alert("Please enter a valid email address.");
		return false;
	}

	if (form.telephone.value == "") {
		alert( "Please enter a valid telephone number." );
		form.telephone.focus();
		return false ;
	}
	if (form.city.value == "") {
		alert( "Please enter your current city." );
		form.city.focus();
		return false ;
	}

	return true;
}

// returns value selected from radio set
function wpss_getCheckedValue(radioObj) {
	if(!radioObj){
  	return "true";
	}
	var radioLength = radioObj.length;
	if(radioLength == undefined)
		if(radioObj.checked)
			return radioObj.value;
		else
			return "";
	for(var i = 0; i < radioLength; i++) {
		if(radioObj[i].checked) {
		  return true;
		}
	}
	return "";
}

function validateEmail(elementValue){  
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;  
    return emailPattern.test(elementValue);  
  }  



(function($) { 
  $(function() {
  
    function controllerKey(event,who, number){
        var code = event.keyCode || event.charCode ;
        // Allow: backspace, delete, tab and escape
        if ( code == 46 || code == 8 || code == 9 || code == 27 || 
             // Allow: Ctrl+A
            (code == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (code >= 35 && code <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            //(code < 96 || code > 105 )
            if ((code < 48 || code > 57)  ) {
                event.preventDefault(); 
                return;
            }   
        }
        if(who.value.length >= number)
          event.preventDefault();
    }
  
    // make sure all required fields are not empty
    jQuery("#mobile_list_item_1,#mobile_list_item_2").keypress(function(event){
      controllerKey(event,this, 3);
    });
      
    jQuery("#mobile_list_item_3").keypress(function(event){
      controllerKey(event, this, 4);
    });
    
    $("#wpssform").submit(function(e){
      var error = false ;
      $("#wpssform .infoForm input.wpss_required").each( function() {
        if($(this).val() == ""){
          alert($(this).attr('alt')+" cannot be blank.");
          error = true;
          e.preventDefault();
          return false;
        }
   	  });
      if(error){
          e.preventDefault();
          return false;
      }
        
      //extra validation
      $name = jQuery(".name_field");
      if($name ){
        if(jQuery.trim($name.val()).length < 4){
            alert($name.attr("alt") + " must have at least 4 characters long");
            e.preventDefault();
            return false;
        }
        else{
          name = document.getElementById("name_field_ex");
          name.value = $name.val();
        }
      }
      
      $email = jQuery(".email_field");
      if($email){
        if(validateEmail($email.val())== false){
          alert($email.attr("alt") + " is not a valid email");
          e.preventDefault();
          return false;
        }
        else{
          email = document.getElementById("email_field_ex");
          email.value = $email.val();
        }
        
      }
      
      
        
      
      
      
      
      
      var mobile = document.getElementById("mobile_list_container");
      if(mobile ){
        
       $item1 = jQuery.trim(jQuery("#mobile_list_item_1").val());
       $item2 = jQuery.trim(jQuery("#mobile_list_item_2").val());
       $item3 = jQuery.trim(jQuery("#mobile_list_item_3").val());
       
       if(/^\d{3}$/.test($item1) == false){
         alert(" The first mobile field must have 3 digits");
         e.preventDefault();
         return false;
       }
       if(/^\d{3}$/.test($item2)== false){
         alert(" The second mobile field must have 3 digits");
         e.preventDefault();
         return false;
       }
       if(/^\d{4}$/.test($item3) == false){
         alert(" The third mobile field must have 4 digits");
         e.preventDefault();
         return false;
       }
        
       mobile.value = $item1 + $item2 +$item3 ;
      }
    });
    
  });
})(jQuery);
