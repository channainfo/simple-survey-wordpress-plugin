<?php
defined('WPSS_URL') or die('Restricted access');
if (!current_user_can('publish_posts')) wp_die( __('You do not have sufficient permissions to access this page.') );


global $wpdb; 


/**
 *  Export all results for a quiz or all fields for a quiz
 */ 
foreach($_POST as $name => $value){
  if(substr($name,0,25)=="wpss_export_full_results_"){
    $quiz_id = substr($name,25);
    $filename='WPSS-Export-Quiz-'.$quiz_id.'_'.date('m.d.y').'.csv';
    $submissions = wpss_get_Submissions($quiz_id);
    /* Set Doc Type as Text for the CSV */
    header("Content-type: application/csv");
    header("Content-Disposition: attachment; filename=$filename");
    header("Pragma: no-cache");
    header("Expires: 0");
                 
    // export csv rows
    echo "Type,Submitter ID,Quiz ID,Question ID,Answer ID,Weight,Question,Choice,Field ID,Field Name,Field Value,Required,IP Address,Submitted At,Total Score"."\r\n";
    foreach($submissions as $results){
      // print each row in comma separated format
      foreach($results as $result){
        echo _wpss_filter($result['type']).',';
        echo _wpss_filter($result['submitter_id']).',';
        echo _wpss_filter($result['quiz_id']).',';
        echo _wpss_filter($result['question_id']).',';                
        echo _wpss_filter($result['answer_id']).',';
        echo _wpss_filter($result['weight']).',';
        echo _wpss_filter($result['question_txt']).',';        
        echo _wpss_filter($result['choice_txt']).',';
        echo _wpss_filter($result['field_id']).',';        
        echo _wpss_filter($result['field_name']).',';
        echo _wpss_filter($result['field_value']).',';                
        echo _wpss_filter($result['required']).',';
        echo _wpss_filter($result['ip_address']).',';
        echo _wpss_filter($result['submitted_at']).',';        
        echo _wpss_filter($result['total_score'])."\r\n";    
      }
    }
    
      
  } 
  elseif(substr($name,0,21)=="wpss_export_userdata_"){
    $quiz_id = substr($name,21);  
    $submissions = wpss_get_Submissions($quiz_id);
    $filename='WPSS-Export-User-Data-Quiz-'.$quiz_id.'_'.date('m.d.y').'.csv';
    /* Set Doc Type as Text for the CSV */
    header("Content-type: application/csv");
    header("Content-Disposition: attachment; filename=$filename");
    header("Pragma: no-cache");
    header("Expires: 0");  

    // export csv rows
    echo "Type,Submitter ID,Quiz ID,Field ID,Field Name,Field Value,Required,IP Address,Submitted At,Total Score"."\r\n";
    foreach($submissions as $results){
      // print each row in comma separated format
      foreach($results as $result){
        if($result['type']=='field'){
          echo _wpss_filter($result['type']).',';      
          echo _wpss_filter($result['submitter_id']).',';
          echo _wpss_filter($result['quiz_id']).',';
          echo _wpss_filter($result['field_id']).',';        
          echo _wpss_filter($result['field_name']).',';
          echo _wpss_filter($result['field_value']).',';                
          echo _wpss_filter($result['required']).',';
          echo _wpss_filter($result['ip_address']).',';
          echo _wpss_filter($result['submitted_at']).',';        
          echo _wpss_filter($result['total_score'])."\r\n"; 
        }   
      }
    }
        
  }
}


?>
