<?php
/**
 *  Builds the HTML quiz outputed to user
 *
 *  @param int $quiz_id
 *  @return string
 *    HTML and Javascript for quiz
 */

function dbug($var){
  echo "<pre>";
  print_r($var);
  echo "</pre>";
}


function wpss_getQuiz($quiz_id){
  global $wpdb; 
  
  $current_plugin = plugin_dir_url(__FILE__) ; 
  $image_url_yes = "{$current_plugin}images/yes.png";
  $image_url_no = "{$current_plugin}images/no.png" ;
 
  // get this quiz and associated questions and answers
  $quiz = stripslashes_deep($wpdb->get_row("SELECT * FROM ".WPSS_QUIZZES_DB." WHERE id='$quiz_id'",ARRAY_A));
  $questions  = stripslashes_deep($wpdb->get_results("SELECT * FROM ".WPSS_QUESTIONS_DB." WHERE quiz_id='$quiz_id' ORDER BY id ASC",ARRAY_A));
  $answer_set = stripslashes_deep($wpdb->get_results("SELECT * FROM ".WPSS_ANSWERS_DB." WHERE quiz_id='$quiz_id' ORDER BY id ASC",ARRAY_A));

  // Don't forget to donate and chip in
  $retQuiz .= '
  <!-- WordPress Simple Survey | Copyright Steele Agency, Inc. (http://steele-agency.com) -->
  <div id="wpss_survey">
    <div id="wpss-quiz-'.$quiz['id'].'" class="form-container ui-helper-clearfix ui-corner-all">
    <h2>'.$quiz['quiz_title'].'</h2>
      <div id="progress"><label id="amount">0%</label>
      <p class="pgress">Progress:</p></div>
      <form id="wpssform" name="wpssform" action="'.WPSS_SUBMIT_RESULTS.'" method="post" >';?>
      
      <?php foreach($questions as $i => $question){

        $retQuiz .= '<div id="panel'.($i+1).'" class="form-panel'; if($i>0){ $retQuiz .= ' ui-helper-hidden';} 
        $retQuiz .= '">';

        $retQuiz .= '
          <fieldset class="ui-corner-all">
  
            <p class="form_question">'.$question['question'].'</p><div class="clear"></div>
            <div class="answer">';

            foreach($answer_set as $j => $answer){
              if($answer['question_id']==$question['id']){
                if($question['type']=="radio"){
                  
                  
                   if(strtolower($answer["answer"]) == "no")
                     $retQuiz .= "<input data-answer='{$answer['id']}' data-question='$i' type='image' src='{$image_url_no}' class='btn_image_no' name='btn_answer_no_q{$i}' id='btn_answer_no_{$answer['id']}' /> " ;
                   else if(strtolower($answer["answer"])== "yes")
                     $retQuiz .= "<input data-answer='{$answer['id']}' data-question='$i' type='image' src='{$image_url_yes}' class='btn_image_yes' name='btn_answer_yes_q{$i}' id='btn_answer_yes_{$answer['id']}' /> " ;
                   
                   // always hide this widget for compatibility  
                   $retQuiz .= '<div class="answer_text"  style="display:none;">
                                <input type="radio" class="wpss_radio" name="wpss_ans_radio_q_'.$i.'" id="answer_'.$answer['id'].'" value="wpss_ans_'.$answer['id'].'" />
                                <label for="answer_'.$answer['id'].'">'.$answer['answer'].'</label>
                              </div>
                           <!-- <div class="clear"></div> -->
                          ';                  
                
                   
                   
                   
                }
                
                
                else{
                   $retQuiz .= '<div class="answer_text" >
                     <input type="checkbox" class="wpss_radio" name="wpss_ans_check_a_'.$j.'" id="answer_'.$answer['id'].'" value="wpss_ans_'.$answer['id'].'" />
                       <label for="answer_'.$answer['id'].'">'.$answer['answer'].'
                         </label></div>
                  <!-- <div class="clear"></div> -->
                  ';                
                }
              }
              
              
              
            }
            
            
            $retQuiz .= '
            </div>
          </fieldset>

        </div>';
      }

      $retQuiz .= '
      <div id="thanks" class="form-panel ui-helper-hidden">

        <fieldset class="ui-corner-all">

        <h3>'.$quiz['submit_txt'].'</h3>
        <input type="hidden" name="quiz_id" value="'.$quiz['id'].'" />  
        <input type="hidden" name="submitter_id" value="'.uniqid("wpss_").'" />';

        // include fields for collecting user data
        if($quiz['record_submit_info']) $retQuiz .= wpss_getUserInfo($quiz['id']);

        $retQuiz .= '        
        <input type="hidden" name="wpss_submit_quiz" value="1" />
        <div style="clear:all"></div>
        <input id="submitButton" type="submit" name="wpss_submit" value="'.$quiz['submit_button_txt'].'" />
        </fieldset>

      </div>
      <!--
      <button id="next">Next &gt;</button>
      <button id="back" disabled="disabled">&lt; Back</button>
      -->
    </form>
    </div>
  </div>
  ';

  // add javascript for panel sliding
  $retQuiz .= wpss_slide_panels(count($questions));
  
  return $retQuiz;
}

function isMobileField(&$name){
  if(strpos( $name,"[mobile]")===false){
    return false;
  }
  $name = str_replace("[mobile]", "", $name);
  return true;
}

function isNameField(&$name){
  if(strpos( $name,"[name]" )===false){
    return false;
  }
  $name = str_replace("[name]", "", $name);
  return true;
}
function isEmailField(&$name){
  if(strpos( $name,"[email]")===false){
    return false;
  }
  $name = str_replace("[email]", "", $name);
  return true;
}




function wpss_getUserInfo($quiz_id){

  global $wpdb;
  global $current_user; get_currentuserinfo();

  $fields = stripslashes_deep($wpdb->get_results("SELECT * FROM ".WPSS_FIELDS_DB." WHERE quiz_id='$quiz_id' ORDER BY id ASC",ARRAY_A));
  if(empty($fields)) return;
    
  $info_form .= '<div id="user_info" class="infoForm">';

  foreach($fields as $field){
    
  
    $class = empty($field['required']) ? '' : 'wpss_required';
    $req   = empty($field['required']) ? '' : '*';

    $name = $field['name'];
    
    $cls = "";

    if(isNameField($name) === true){
      $cls = " name_field ";
      $info_form .= "<input type='hidden' name='name_field' id='name_field_ex' value='' /> ";
    }
    if(isEmailField($name) === true){
      $cls = " email_field ";
      $info_form .= "<input type='hidden' name='email_field' id='email_field_ex' value='' /> ";
    }
      
    
    if(isMobileField($name)=== true){
      
      
      $info_form .= '<div class="wpss_customfield">
                     <label>'.$req.$name.'</label>
                       
                     <input type="text" id="mobile_list_item_1" name="mobile_list_item[]" class="field_mobile_list '.$class.'"  value="" alt="'.$name.'" />
                     <input type="text" id="mobile_list_item_2" name="mobile_list_item[]" class="field_mobile_list '.$class.'" value="" alt="'.$name.'" />
                     <input type="text" id="mobile_list_item_3" name="mobile_list_item[]" class="field_mobile_list '.$class.'" value="" alt="'.$name.'" />
                       
                   <div class="clear">
                   </div>
                   </div>';
      
      
      $info_form .= '<div class="wpss_customfield" style="display:none;" >
                     <label>'.$req.$name.'</label>
                     <input size="16" class="field_text" type="text" id="mobile_list_container" name="wpss_field_'.$field['id'].'" value="" alt="'.$name.'" />
                   <div class="clear">
                   </div>
                   </div>';
    }
    else{
      $info_form .= '<div class="wpss_customfield">
                     <label>'.$req.$name.'</label>
                     <input  size="16" type="text" name="wpss_field_'.$field['id'].'" class="field_text '.$class." ".$cls.' " value="" alt="'.$name.'" />
                   <div class="clear">
                   </div>
                   </div>';
      
    }
    $info_form .= '<div class="clear"></div>';
  }
  
  
  $info_form .= '</div>';
  
  
  
  return $info_form;  
}


?>
